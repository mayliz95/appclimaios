//
//  Servicio.swift
//  App1
//
//  Created by Laboratorio FIS on 7/11/17.
//  Copyright © 2017 Laboratorio FIS. All rights reserved.
//

import Foundation

class Servicio {
    
    func consultaPorCiudad(city: String, completion:@escaping (String) -> ()) {
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city);uk&appid=f41020d6f457c1bf6a9ec1556b596a7b"
        consulta(urlStr: urlStr) { (weather) in
            completion(weather)
        }
    }
    
    func consultaPorUbicacion(lat: Double, lon: Double, completion:@escaping (String) -> ()) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=7d95e6fefc2b7875252004fead0caaeb"
        consulta(urlStr: urlString) { (weather) in
            completion(weather)
        }
    }
    
    func consulta(urlStr: String, completion:@escaping (String) ->()) {
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                return
            }
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDict = weatherJson as! NSDictionary
                guard let weatherKey = weatherDict["weather"] as? NSArray else{
                    completion("Ciudad no válida")
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                completion("\(weather["description"] ?? "Error")")
            }
            catch{
                print("Error al generar el Json")
                
            }
        }
        task.resume()
    }
}
