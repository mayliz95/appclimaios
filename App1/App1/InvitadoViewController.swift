import UIKit
import CoreLocation

class InvitadoViewController: UIViewController, CLLocationManagerDelegate{
    
    //MARK:- Outlets
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var nombreCiudadL: UILabel!
    
    let locationManager = CLLocationManager()
    var bandera=false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        if !bandera {
        consultarButtonPressed(
                lat: (location?.latitude)!,
                lon: (location?.longitude)!
        )
            bandera = true
        }
    }
    
    private func consultarButtonPressed(lat: Double, lon: Double ) {
        let service = Servicio()
        
        service.consultaPorUbicacion(lat: lat, lon: lon) { (clima) in
            DispatchQueue.main.async {
            self.Label.text = clima
        }
    }
    
    }
}
    

    


