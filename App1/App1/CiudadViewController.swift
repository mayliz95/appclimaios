//
//  CiudadViewController.swift
//  App1
//
//  Created by Laboratorio FIS on 31/10/17.
//  Copyright © 2017 Laboratorio FIS. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {

    //MARK:- Outlets
    
    
    @IBOutlet weak var CuidadTextField: UITextField!
    
    @IBOutlet weak var cliemaLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        //http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=76f9031b8d973f7baa9eeebe8f0ca5e7
        
        let service = Servicio()
        //service.consultaPorCiudad(city: CuidadTextField.text!,)
        service.consultaPorCiudad(city: CuidadTextField.text!) { (clima) in
            DispatchQueue.main.async {
                self.cliemaLabel.text = clima
            }
        }
        
    }
    

    
    

}
